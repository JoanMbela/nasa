console.log("NASA")

const imgs_url = "https://images-api.nasa.gov/search?description=moon&media_type=image&page=1"
const vids_url = "https://images-api.nasa.gov/search?description=moon&media_type=video"
var i = 0

// "https://api.nasa.gov/planetary/apod?api_key=rzCb3Wsjlgp4dIoa0fgpt5XSHKlDcdKwxzOSzmmm"

var collection = null

axios
    .get(imgs_url)
    .then(function (res) { collection = res.data.collection.items; loadImage() })
    
function loadImage(c = collection, index = i) {

    // console.log(res.data)

    const img = document.querySelector('img')
    img.style.maxHeight = '70vh'
    img.style.width = 'auto'
    img.src = c[index].links[0].href

}

function nextImage() {
    i++;
    loadImage()
}

function prevImage() {
    i--;
    loadImage()
}
